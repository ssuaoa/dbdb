package com.example.daisy.sketchboard;

import java.util.ArrayList;
import java.util.LinkedList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class MyView2 extends View implements OnTouchListener {
    //필요한 멤버필드 정의하기
    //상태값을 상수로 정의
    static final int RED_STATE = 0;
    static final int BLUE_STATE = 1;
    static final int YELLOW_STATE = 2;
    //현재 색의 상태값을 저장할 변수
    int colorState = RED_STATE;
    //Paint 객체를 저장할 배열 객체 생성하기
    //Paint[] paintList = new Paint[3];
    Canvas canvas;
    Paint redPaint;
    Path path;
    private LinkedList<Path> paths = new LinkedList<Path>();

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    ArrayList<Point2> points = new ArrayList<Point2>();

    public MyView2(Context context) {
        super(context);
        init();//초기화

        setFocusable(true);
        this.setOnTouchListener(this);
        setFocusableInTouchMode(true);
    }

    //xml에 view를 추가시 인자2개짜리 생성자가 필요하다!!!!!!!!!!!
    public MyView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();//초기화

        setFocusable(true);
        this.setOnTouchListener(this);
        setFocusableInTouchMode(true);

    }

    //초기화 하는 메소드
    public void init() {
        //arraylist 객체 생성하기
        points = new ArrayList<Point2>();
        //선을 그림 Paint 객체 생성 및 초기화
        redPaint = new Paint();
        redPaint.setAntiAlias(true);
        redPaint.setDither(true);
        redPaint.setColor(Color.BLACK);
        redPaint.setStyle(Paint.Style.STROKE);
        redPaint.setStrokeJoin(Paint.Join.ROUND);
        redPaint.setStrokeCap(Paint.Cap.ROUND);
        redPaint.setStrokeWidth(30);

        canvas = new Canvas();
        path = new Path();
        paths.add(path);
    }

    //화면을 그리는 메소드
    @Override
    public void onDraw(Canvas canvas) {
        /*

        path = new Path();

        if (points.size() > 1) {
            for (int i = points.size() - 2; i < points.size(); i++) {
                if (i >= 0) {
                    Point2 point = points.get(i);

                    if (i == 0) {
                        Point2 next = points.get(i + 1);
                        point.dx = ((next.x - point.x) / 3);
                        point.dy = ((next.y - point.y) / 3);
                    } else if (i == points.size() - 1) {
                        Point2 prev = points.get(i - 1);
                        point.dx = ((point.x - prev.x) / 3);
                        point.dy = ((point.y - prev.y) / 3);
                    } else {
                        Point2 next = points.get(i + 1);
                        Point2 prev = points.get(i - 1);
                        point.dx = ((next.x - prev.x) / 3);
                        point.dy = ((next.y - prev.y) / 3);
                    }
                }
            }
        }

        boolean first = true;
        for (int i = 0; i < points.size(); i++)

        {
            Point2 point = points.get(i);
            if (first) {
                first = false;
                path.moveTo(point.x, point.y);
            } else {
                Point2 prev = points.get(i - 1);
                path.cubicTo(prev.x + prev.dx, prev.y + prev.dy, point.x - point.dx, point.y - point.dy, point.x, point.y);
            }
        }

        canvas.drawPath(path, redPaint);
    }
    */


        for (Path p : paths) {
            canvas.drawPath(p, redPaint);
        }

    }


    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        Point2 point = new Point2();
        point.x = event.getX();
        point.y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.reset();
                path.moveTo(point.x, point.y);
                mX = point.x;
                mY = point.y;
                break;
            case MotionEvent.ACTION_MOVE:
                float dx = Math.abs(point.x - mX);
                float dy = Math.abs(point.y - mY);
                if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                    path.quadTo(mX, mY, (point.x + mX) / 2, (point.y + mY) / 2);
                    mX = point.x;
                    mY = point.y;
                }
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                path.lineTo(mX, mY);
                // commit the path to our offscreen
                canvas.drawPath(path, redPaint);
                // kill this so we don't double draw
                path = new Path();
                paths.add(path);
                invalidate();
                break;
        }
        return true;
    }
}
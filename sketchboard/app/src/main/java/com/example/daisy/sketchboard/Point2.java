package com.example.daisy.sketchboard;

/**
 * Created by Daisy on 2015-02-17.
 */
public class Point2 {
    float x, y;
    float dx, dy;

    @Override
    public String toString() {
        return x + ", " + y;
    }
}
package com.example.jeong_min.myapplication;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.ImageButton;

import static com.example.jeong_min.myapplication.R.layout.test;
import static com.example.jeong_min.myapplication.R.layout.notmal_select;

/**
 * Created by jeong-min on 2015-01-26.
 */



public class Menu1 extends Activity implements View.OnClickListener  {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(test);

        ImageButton button2=(ImageButton)findViewById(R.id.imageButton2);
        button2.setOnClickListener(this);

        ImageButton button3=(ImageButton)findViewById(R.id.imageButton3);
        button3.setOnClickListener(this);

    }

    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.imageButton2:
            {

                Intent intent = new Intent(this, WordSelect.class);
                startActivity(intent);

                break;
            }

            case R.id.imageButton3:
            {

                Intent intent2 = new Intent(this, RealWordSelect.class);
                startActivity(intent2);

                break;
            }

        }
    }

}


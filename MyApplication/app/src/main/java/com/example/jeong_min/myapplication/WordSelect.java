package com.example.jeong_min.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import static com.example.jeong_min.myapplication.R.layout.only_letter_select;

/**
 * Created by jeong-min on 2015-02-18.
 */
public class WordSelect extends Activity implements View.OnClickListener {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(only_letter_select);

        ImageButton button3=(ImageButton)findViewById(R.id.imageButton6);
        button3.setOnClickListener(this);

        ImageButton button4=(ImageButton)findViewById(R.id.imageButton7);
        button4.setOnClickListener(this);


    }
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.imageButton6:
            {

                Intent intent = new Intent(this, Word1.class);
                startActivity(intent);

                break;
            }
            case R.id.imageButton7:
            {

                Intent intent = new Intent(this, Word1.class);
                startActivity(intent);

                break;
            }
        }
    }
}

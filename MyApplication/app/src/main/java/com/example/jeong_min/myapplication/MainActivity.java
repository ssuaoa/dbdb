package com.example.jeong_min.myapplication;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Toast;

import static com.example.jeong_min.myapplication.R.layout.activity_main;


public class MainActivity extends Activity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);

        Button button=(Button)findViewById(R.id.ok_button);
        button.setOnClickListener(this);

    }

    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.ok_button:
            {

                Intent intent = new Intent(this, Menu1.class);
                startActivity(intent);

                break;
            }
        }
    }


}

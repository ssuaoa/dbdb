package com.example.jeong_min.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import static com.example.jeong_min.myapplication.R.layout.letter_level_select;

/**
 * Created by Daisy on 2015-07-20.
 */
public class RealWordSelect extends Activity implements View.OnClickListener{

    ImageButton button26;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(letter_level_select);

        button26 = (ImageButton) findViewById(R.id.imageButton26);
        button26.setSelected(false);
        button26.setOnClickListener(this);
    }


    public void onClick(View v)
    {
        switch(v.getId()) {

            case R.id.imageButton26: {

                if (!(button26.isSelected()))
                {
                    button26.setSelected(true);
                }
                else
                {
                    button26.setSelected(false);
                }


               /*
               Intent intent = new Intent(this, Word1.class);
                startActivity(intent);
                */

                break;
            }
        }
    }

}



package com.example.jeong_min.myapplication;

/**
 * Created by Daisy on 2015-02-17.
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.larvalabs.svgandroid.SVG;
import com.larvalabs.svgandroid.SVGParser;

public class Drawing extends Activity {
    /** Called when the activity is first created. */
    MyView myView;

    //외부 라이브러리 추가
    //이렇게 하는거 맞니...
    //png->vec
    // tor
    SVG svg = SVGParser.getSVGFromResource(getResources(),R.drawable.ga);
    Picture picture = svg.getPicture();
    Drawable drawable = svg.createPictureDrawable();


    //버튼에 상태 표시하기
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(new MyView(this));
        setContentView(R.layout.test_layout);
        //Myview 객체의 참조값 얻어오기
        myView = (MyView)findViewById(R.id.myView);
        //버튼 객체의 참조값 얻어오기
        // redBtn = (Button)findViewById(R.id.red);
        //blueBtn = (Button)findViewById(R.id.blue);
        //yellowBtn = (Button)findViewById(R.id.yellow);




    }

    //버튼을 눌렀을 때 호출되는 메소드
    //버튼도 각각 하나의 view이다 view에 있는 상대값을 알아오는 방법은 id를 등록하고 R.id로 접근하는 방법이였다
    //마찬가지로 MyView에 있는 참조값을 얻어오려면 id로 등록하면 된다.
    public void changeColor(View v){
        //버튼의 글자색 초기화
        // redBtn.setTextColor(Color.BLACK);
        //blueBtn.setTextColor(Color.BLACK);
        //yellowBtn.setTextColor(Color.BLACK);

        switch(v.getId()){
            //MyView에 있는 파이널 스태틱 상수값을 MyView에 있는 colorState 변수에 집어넣는 것.
            // case R.id.red : myView.colorState=MyView.RED_STATE; redBtn.setTextColor(Color.RED); break;
            // case R.id.blue : myView.colorState=MyView.BLUE_STATE; blueBtn.setTextColor(Color.BLUE); break;
            // case R.id.yellow : myView.colorState=MyView.YELLOW_STATE; yellowBtn.setTextColor(Color.YELLOW);break;

        }
    }
    //옵션 메뉴 만들기 위해서 오버라이딩 한다.(옵션메뉴는 한화면에 최대6개, 넘어가는 것은 더보기 메뉴가 생긴다)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //인자로 전달된 Menu 객체에 아이템을 추가한다.(?, 아이디역할, 그룹, "메뉴명")
        menu.add(0, 1, 0, "저장");
        menu.add(0, 2, 0, "불러오기");
        menu.add(0, 3, 0, "삭제");
        menu.add(0, 4, 0, "끝내기");

        return super.onCreateOptionsMenu(menu);
    }
    //옵션 메뉴 선택시 기능을 추가하기 위해서 오버라이딩 한다.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //item객체에 어느 옵션메뉴를 클릭했는지에 대한 정보가 있다.
        switch(item.getItemId()){
            case 1://저장하기를 선택했을때 list를 파일로 저장한다(ObjectoutputStream) : 클래서에 대한 정보를 담는다.
                //ArrayList를 담기 위해서는 point를 serializable(직렬화)을 implements해주어야만한다(자바는 되있는데 안드로이드는 안되있다)
                //해당 클래스(Paint or ArrayList 를 클릭해서 클래스 정보를 보면 직렬화를 임플리먼츠 했는지 안했는지 알 수 있다)
                try {
                    FileOutputStream fos = openFileOutput("picture.dat", Context.MODE_WORLD_WRITEABLE);
                    //객체를 파일에 저장하기 위해 objectOutputStream 객체로 감싼다.
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    //Myview의 멤버필드 즉 ArrayList를 저장한다.
                    oos.writeObject(myView.points);

                    oos.close();
                   // Toast.makeText(this, "저장성공!", 0).show();
                } catch (Exception e) {
                    Log.e("저장실패:", e.getMessage());
                 //   Toast.makeText(this, "저장실패!", 0).show();
                }
                break;
            case 2: //불러오기
                try{//파일에서 읽어오기 위한 스트림 객체 얻어오기(경로는 써주지 않아도 된다, 약속된 위치가 있기 때문)
                    FileInputStream fis = openFileInput("picture.dat");
                    //객체를 읽어들이기 위해서 objectInputStream으로 감싼다
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    //객체를 읽어와서 원래 데이터 형태로 형변환한다.
                    ArrayList<Point> readedObject = (ArrayList<Point>)ois.readObject();
                    //읽어온 객체를 MyView에 전달한다.
                    myView.points = readedObject;
                    //화면을 갱신해서 현재 객체의 정보를 바탕으로 화면을 그린다.
                    myView.invalidate();
                    Toast.makeText(this, "불러오기성공!", 0).show();
                }catch(Exception e){
                    Log.e("불러오기실패:", e.getMessage());
                    Toast.makeText(this, "불러오기실패!", 0).show();
                }
                break;
            case 3: //삭제는 따로 하시길!
                break;
            case 4: //종료하기를 선택했을때
                finish(); //액티비티를 종료시킨다.
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}